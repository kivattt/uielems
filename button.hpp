// button.hpp
/* kivattt 2019
 * https://gitlab.com/kivattt/uilems
 * uielems - User Interface elements
*/

#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <SFML/Graphics.hpp>
#include <iostream>

#include "sfmlextensionminimal.hpp"

struct Button{
	sf::Text titleText;
	sf::RectangleShape boxBoundary;
	unsigned char charSize=14;
	unsigned x=0;
	unsigned y=0;
	unsigned char lineWidth=1;
	unsigned char boundarySpace=6;

	sf::Color textColor{255,255,255}, backgroundColor{40,48,50}, activeColor{30,34,48}, inactiveColor{55,66,100};
	bool active=false;

	Button(){
		// It's up to the user to set the title text and font
		titleText.setString("UNASSIGNED_TITLE");
		update_values();
	}

	void update_values(){
		titleText.setPosition(x + boundarySpace, y + boundarySpace);
		titleText.setCharacterSize(charSize);
		titleText.setFillColor(textColor);

		boxBoundary.setPosition(x,y);
		boxBoundary.setSize(sf::Vector2f(titleText.getGlobalBounds().width + boundarySpace*2, titleText.getGlobalBounds().height + boundarySpace*2));
		boxBoundary.setOutlineThickness(lineWidth);
		boxBoundary.setOutlineColor(inactiveColor);
		boxBoundary.setFillColor(backgroundColor);
	}

	SFMLExtension extension;
	void update(sf::RenderWindow &window){
		const auto mousePos = sf::Mouse::getPosition(window);
		if (extension.m1_pressed() && boxBoundary.getGlobalBounds().contains(mousePos.x, mousePos.y)) // Clicked
			active=true;
		else
			active=false;
	}

	void draw(sf::RenderWindow &window){
		if (active)
			boxBoundary.setOutlineColor(activeColor);
		else
			boxBoundary.setOutlineColor(inactiveColor);

		window.draw(boxBoundary);
		window.draw(titleText);
	}
};

struct TogglingButton{
	sf::Text titleText;
	sf::RectangleShape boxBoundary;
	unsigned char charSize=14;
	unsigned x=0;
	unsigned y=0;
	unsigned char lineWidth=1;
	unsigned char boundarySpace=6;

	sf::Color textColor{255,255,255}, backgroundColor{37,42,45}, activeColor{30,34,48}, inactiveColor{55,66,100};
	bool toggled=false;
	bool active=false;

	TogglingButton(){
		// It's up to the user to set the title text and font
		titleText.setString("UNASSIGNED_TITLE");
		update_values();
	}

	void update_values(){
		titleText.setPosition(x + boundarySpace, y + boundarySpace);
		titleText.setCharacterSize(charSize);
		titleText.setFillColor(textColor);

		boxBoundary.setPosition(x,y);
		boxBoundary.setSize(sf::Vector2f(titleText.getGlobalBounds().width + boundarySpace*2, titleText.getGlobalBounds().height + boundarySpace*2));
		boxBoundary.setOutlineThickness(lineWidth);
		boxBoundary.setOutlineColor(inactiveColor);
		boxBoundary.setFillColor(backgroundColor);
	}

	SFMLExtension extension;
	void update(sf::RenderWindow &window){
		const auto mousePos = sf::Mouse::getPosition(window);
		if (extension.m1_pressed() && boxBoundary.getGlobalBounds().contains(mousePos.x, mousePos.y)){ // Clicked
			active^=1; // Toggle
			toggled = true;
		} else{
			toggled = false;
		}
	}

	void draw(sf::RenderWindow &window){
		if (active)
			boxBoundary.setOutlineColor(activeColor);
		else
			boxBoundary.setOutlineColor(inactiveColor);

		window.draw(boxBoundary);
		window.draw(titleText);
	}
};

#endif // UI_BUTTON
