// textbox.hpp
/* kivattt 2019
 * https://gitlab.com/kivattt/uilems
 * uielems - User Interface elements
*/

#ifndef TEXTBOX_HPP
#define TEXTBOX_HPP

#include <SFML/Graphics.hpp>
#include <string>

using std::string;

class TextBox{
	bool unicode_char_is_printable(const unsigned character){
		return (character>=32) && (character<=126);
	}

	char lastCharTyped=0; // 0 for no characer, 1 for backspace

	void updateBoundarySize(){
		boxBoundary.setSize(sf::Vector2f(text.getGlobalBounds().width + boundarySpace*2, text.getGlobalBounds().height + boundarySpace*2));
	}

	string get_str_without_last_char(const string str){
		if (str.empty()) return str;
		return str.substr(0,str.size()-1);
	}

	public:

	sf::Text text;
	sf::RectangleShape boxBoundary;
	unsigned char charSize=14;
	unsigned x=0;
	unsigned y=0;
	unsigned char lineWidth=1;
	unsigned char boundarySpace=6;

	sf::Color textColor{255,255,255}, backgroundColor{40,48,50}, boundaryColor{30,34,48};

	TextBox(){
		// It's up to the user to set the text and font
		text.setString("");
		update_values();
	}

	void update_values(){
		text.setPosition(x + boundarySpace, y + boundarySpace);
		text.setCharacterSize(charSize);
		text.setFillColor(textColor);

		boxBoundary.setPosition(x,y);
		updateBoundarySize();
		boxBoundary.setOutlineThickness(lineWidth);
		boxBoundary.setOutlineColor(boundaryColor);
		boxBoundary.setFillColor(backgroundColor);
	}

	// If the mouse is inside the textbox and text is entered, add the text
	// Refer to ~/main/unicodechars for printable and other chars
	// TODO also make it have a cursor that can be manipulated with the arrow keys (the arrow keys are not unicode characters, handle them separately like sf::Keyboard::Left or something)
	void update(sf::RenderWindow &window, const sf::Event e){
		const auto mousePos = sf::Mouse::getPosition(window);
		if (boxBoundary.getGlobalBounds().contains(mousePos.x, mousePos.y)){
			if (e.type == sf::Event::TextEntered){
				const unsigned character = e.text.unicode;
				if (character==8){ // Backspace
					if (lastCharTyped != 1){
						text.setString(get_str_without_last_char(text.getString()));
						updateBoundarySize();
					}
					lastCharTyped = 1;
					return;
				}

				if (!unicode_char_is_printable(character))
					return;

				// Add entered character
				if (char(character) != lastCharTyped){
					text.setString(text.getString() + char(character));
					updateBoundarySize();
				}
				lastCharTyped = char(character);

				return;
			}
		}
		lastCharTyped = 0;
	}

	void draw(sf::RenderWindow &window){
		window.draw(boxBoundary);
		window.draw(text);
	}
};

#endif // TEXTBOX_HPP
